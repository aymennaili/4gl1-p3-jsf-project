package tn.esprit.gl1.p3.ejb.service.generic;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

public interface GenericService<T, ID extends Serializable> {
	
	EntityManager getEntityManager();
	void setEntityManager(EntityManager em);
	
	//Find (or retrieve) methods
	T findOneByID(ID id);
	Collection<T> findAll();
	Collection<T> findManyByExample(T example);
	Collection<T> findManyByCriteria(CriteriaQuery<T> criteria);
	Collection<T> findManyByQuery(String jpaQuery);
	
	//Create methods
	boolean create(T entity);
	boolean createMany(Collection<T> setOfEntities);
	
	//Update methods
	boolean update(T newEntity);
	boolean updateMany(Collection<T> entities);
	
	//Delete methods
	boolean delete(T entity);
	boolean delete(ID id);
	boolean deleteMany(Collection<T> entitiesToDelete);
	boolean deleteManyByID(Collection<ID> ids);
	boolean deleteAll();
}
