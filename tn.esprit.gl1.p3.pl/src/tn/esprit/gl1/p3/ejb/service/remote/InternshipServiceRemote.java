package tn.esprit.gl1.p3.ejb.service.remote;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.gl1.p3.dal.entity.CurriculumVitae;
import tn.esprit.gl1.p3.dal.entity.Internship;
import tn.esprit.gl1.p3.ejb.service.generic.GenericService;

@Remote
public interface InternshipServiceRemote extends GenericService<Internship, Long> {
	public List<Internship> findByCv(CurriculumVitae cv);
}
