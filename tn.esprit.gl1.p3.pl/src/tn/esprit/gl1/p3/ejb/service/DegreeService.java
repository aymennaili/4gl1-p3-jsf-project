package tn.esprit.gl1.p3.ejb.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import tn.esprit.gl1.p3.dal.entity.CurriculumVitae;
import tn.esprit.gl1.p3.dal.entity.Degree;
import tn.esprit.gl1.p3.dal.entity.Degree_;
import tn.esprit.gl1.p3.ejb.service.generic.AbstractGenericService;
import tn.esprit.gl1.p3.ejb.service.remote.DegreeServiceRemote;

@Stateless
@LocalBean
public class DegreeService extends AbstractGenericService<Degree, Long> implements DegreeServiceRemote{

	@PersistenceContext
	private EntityManager em;
	
	Logger log = Logger.getLogger(DegreeService.class.getName());
	
	@Override
	public List<Degree> findByCv(CurriculumVitae cv) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Degree> query = cb.createQuery(Degree.class);
		Root<Degree> root = query.from(Degree.class);
		Predicate p = cb.equal(root.get(Degree_.CV),cv);
		query.where(p);
		List<Degree> resultList = em.createQuery(query).getResultList();
		log.info("Rerieved "+resultList.size()+"Degree for the cv  "+cv.getCurriculumId());
		return resultList;
	}
	
}
