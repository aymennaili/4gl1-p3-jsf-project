package tn.esprit.gl1.p3.ejb.service.remote;

import javax.ejb.Remote;

import tn.esprit.gl1.p3.dal.entity.Profile;
import tn.esprit.gl1.p3.ejb.service.generic.GenericService;

@Remote
public interface ProfileServiceRemote extends GenericService<Profile, Long>{
	public Profile findByEmailAndPassword(String email,String password);
//	public List<Profile> findAllProfiles();
//	public boolean deleteProfile (Profile p);
//	public boolean delete(Profile p);
}
