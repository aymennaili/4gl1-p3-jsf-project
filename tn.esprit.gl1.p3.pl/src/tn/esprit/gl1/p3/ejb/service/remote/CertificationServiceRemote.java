package tn.esprit.gl1.p3.ejb.service.remote;

import javax.ejb.Remote;

import tn.esprit.gl1.p3.dal.entity.Certification;
import tn.esprit.gl1.p3.ejb.service.generic.GenericService;

@Remote
public interface CertificationServiceRemote extends GenericService<Certification, Long>{

}
