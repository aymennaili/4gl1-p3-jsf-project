package tn.esprit.gl1.p3.ejb.service.remote;

import javax.ejb.Remote;

import tn.esprit.gl1.p3.dal.entity.Event;
import tn.esprit.gl1.p3.ejb.service.generic.GenericService;

@Remote
public interface EventServiceRemote extends GenericService<Event, Long>{

}
