package tn.esprit.gl1.p3.ejb.service;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import tn.esprit.gl1.p3.dal.entity.Event;
import tn.esprit.gl1.p3.ejb.service.generic.AbstractGenericService;
import tn.esprit.gl1.p3.ejb.service.remote.EventServiceRemote;

@Stateless
@LocalBean
public class EventService extends AbstractGenericService<Event, Long> implements EventServiceRemote {

}