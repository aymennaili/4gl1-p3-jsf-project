package tn.esprit.gl1.p3.ejb.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.gl1.p3.dal.entity.Company;
import tn.esprit.gl1.p3.ejb.service.generic.AbstractGenericService;
import tn.esprit.gl1.p3.ejb.service.remote.CompanyServiceRemote;

@Stateless
@LocalBean
public class CompanyService extends AbstractGenericService<Company, Long> implements CompanyServiceRemote {
	
Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public Company findOneByEmail(String email) {
		
		log.info("Getting Company instance with email : "+email);
		List<Company> resultList = em.createQuery(CompanyServiceRemote.findByEmailQuery)
				.setParameter("email", email).getResultList();
		Company result = resultList.size() == 1 ? resultList.get(0) : null;
		if (null == result)
			log.warning("Company with email : " + email
					+ " not found in the database -- returning null");
		else
			log.info("Returning Company instance with ID : "+result.getProfileId());
		return result;
		
		
	}

}
