package tn.esprit.gl1.p3.ejb.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.gl1.p3.dal.entity.Student;
import tn.esprit.gl1.p3.ejb.service.generic.AbstractGenericService;
import tn.esprit.gl1.p3.ejb.service.remote.StudentServiceRemote;

@Stateless
@LocalBean
public class StudentService extends AbstractGenericService<Student, Long> implements
		StudentServiceRemote {

	Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public Student findOneByEmail(String email) {
		log.info("Getting Student instance with email : "+email);
		List<Student> resultList = em.createQuery(StudentServiceRemote.findByEmailQuery)
				.setParameter("email", email).getResultList();
		Student result = resultList.size() == 1 ? resultList.get(0) : null;
		if (null == result)
			log.warning("Student with email : " + email
					+ " not found in the database -- returning null");
		else
			log.info("Returning Student instance with ID : "+result.getProfileId());
		return result;
	}
	
	

}
