package tn.esprit.gl1.p3.ejb.service.remote;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.gl1.p3.dal.entity.Student;

import tn.esprit.gl1.p3.ejb.service.generic.GenericService;

@Remote
public interface StudentServiceRemote extends GenericService<Student, Long>{
	String findByEmailQuery = "select s from Student s where s.email = :email";
	
	Student findOneByEmail(String email);
	
}
