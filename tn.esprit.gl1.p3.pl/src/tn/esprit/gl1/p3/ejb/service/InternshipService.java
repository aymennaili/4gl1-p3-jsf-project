package tn.esprit.gl1.p3.ejb.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import tn.esprit.gl1.p3.dal.entity.CurriculumVitae;
import tn.esprit.gl1.p3.dal.entity.Internship;
import tn.esprit.gl1.p3.dal.entity.Internship_;
import tn.esprit.gl1.p3.ejb.service.generic.AbstractGenericService;
import tn.esprit.gl1.p3.ejb.service.remote.InternshipServiceRemote;

@Stateless
@LocalBean
public class InternshipService extends AbstractGenericService<Internship, Long>
		implements InternshipServiceRemote {

	Logger log = Logger.getLogger(InternshipService.class.getName());
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Internship> findByCv(CurriculumVitae cv) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Internship> query = cb.createQuery(Internship.class);
		Root<Internship> root = query.from(Internship.class);
		Predicate p = cb.equal(root.get(Internship_.CV), cv);
		query.where(p);
		List<Internship> resultList = em.createQuery(query).getResultList();
		log.info("Rerieved " + resultList.size() + "Internship for the cv  "
				+ cv.getCurriculumId());
		return resultList;
	}
	
}
