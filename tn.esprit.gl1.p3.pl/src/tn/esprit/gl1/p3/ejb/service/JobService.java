package tn.esprit.gl1.p3.ejb.service;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import tn.esprit.gl1.p3.dal.entity.Job;
import tn.esprit.gl1.p3.ejb.service.generic.AbstractGenericService;
import tn.esprit.gl1.p3.ejb.service.remote.JobServiceRemote;

@Stateless
@LocalBean
public class JobService extends AbstractGenericService<Job, Long> implements JobServiceRemote{

}
