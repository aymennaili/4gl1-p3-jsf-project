package tn.esprit.gl1.p3.ejb.service.remote;

import javax.ejb.Remote;

import tn.esprit.gl1.p3.dal.entity.Company;
import tn.esprit.gl1.p3.dal.entity.Student;
import tn.esprit.gl1.p3.ejb.service.generic.GenericService;

@Remote
public interface CompanyServiceRemote extends GenericService<Company, Long>{
	
String findByEmailQuery = "select s from Company s where s.email = :email";
	
	Company findOneByEmail(String email);
    
}
