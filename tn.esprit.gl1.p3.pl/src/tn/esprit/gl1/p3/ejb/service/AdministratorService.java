package tn.esprit.gl1.p3.ejb.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import tn.esprit.gl1.p3.dal.entity.Administrator;
import tn.esprit.gl1.p3.dal.entity.Administrator_;
import tn.esprit.gl1.p3.dal.entity.Profile;
import tn.esprit.gl1.p3.dal.entity.Profile_;
import tn.esprit.gl1.p3.ejb.service.generic.AbstractGenericService;
import tn.esprit.gl1.p3.ejb.service.remote.AdministratorServiceRemote;
@Stateless
@LocalBean
public class AdministratorService extends AbstractGenericService<Administrator, Long> implements AdministratorServiceRemote{


}
