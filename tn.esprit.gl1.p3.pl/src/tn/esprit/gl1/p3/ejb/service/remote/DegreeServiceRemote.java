package tn.esprit.gl1.p3.ejb.service.remote;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.gl1.p3.dal.entity.CurriculumVitae;
import tn.esprit.gl1.p3.dal.entity.Degree;
import tn.esprit.gl1.p3.ejb.service.generic.GenericService;

@Remote
public interface DegreeServiceRemote extends GenericService<Degree, Long>{
	public List<Degree> findByCv(CurriculumVitae cv);
}
