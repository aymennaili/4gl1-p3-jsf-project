package tn.esprit.gl1.p3.ejb.service.remote;

import javax.ejb.Remote;

import tn.esprit.gl1.p3.dal.entity.Aspiration;
import tn.esprit.gl1.p3.ejb.service.generic.GenericService;
@Remote
public interface AspirationServiceRemote extends GenericService<Aspiration, Long>{
	
}
