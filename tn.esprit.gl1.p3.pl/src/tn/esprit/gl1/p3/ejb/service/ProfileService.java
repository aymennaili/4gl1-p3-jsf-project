package tn.esprit.gl1.p3.ejb.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import tn.esprit.gl1.p3.dal.entity.Profile;
import tn.esprit.gl1.p3.dal.entity.Profile_;
import tn.esprit.gl1.p3.ejb.service.generic.AbstractGenericService;
import tn.esprit.gl1.p3.ejb.service.remote.ProfileServiceRemote;

@Stateless
@LocalBean
public class ProfileService extends AbstractGenericService<Profile, Long>
		implements ProfileServiceRemote {

	@PersistenceContext
	private EntityManager em;

	public ProfileService() {
	}

	@Override
	public Profile findByEmailAndPassword(String email, String password) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Profile> query = cb.createQuery(Profile.class);
		Root<Profile> root = query.from(Profile.class);
		Predicate pEmail = cb.equal(root.get(Profile_.email), email);
		Predicate pPassword = cb.equal(root.get(Profile_.password), password);
		query.where(pEmail, pPassword);
		List<Profile> resultSet = getEntityManager().createQuery(query)
				.getResultList();
		if (resultSet.size() == 1)
			return resultSet.get(0);
		return null;
	}

/*	@Override
	public List<Profile> findAllProfiles() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Profile> query = cb.createQuery(Profile.class);
		Root<Profile> root = query.from(Profile.class);
		List<Profile> resultSet = getEntityManager().createQuery(query)
				.getResultList();
		return (List<Profile>) resultSet;
	}*/

	/*@Override
	public boolean deleteProfile(Profile p) {
		try {
			em.remove(em.merge(p));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}*/

}
