package tn.esprit.gl1.p3.ejb.service.generic;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Stateless
@LocalBean
public abstract class AbstractGenericService<T, ID extends Serializable>
		implements GenericService<T, ID> {

	@PersistenceContext
	private EntityManager em;

	// private Session session;
	private Class<T> persistentClass;
	
	public AbstractGenericService() {
		this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	@Override
	public T findOneByID(ID id) {
		return em.find(persistentClass, id);
	}

	@Override
	public Collection<T> findAll() {
		CriteriaQuery<T> query = em.getCriteriaBuilder().createQuery(persistentClass);
		Root<T> root=query.from(persistentClass);
		Collection<T> records = em.createQuery(query).getResultList();
		return records;
	}

	@Override
	public Collection<T> findManyByExample(T example) {
		// Criteria criteria = session.createCriteria(persistentClass);
		// criteria.add(Example.create(example));
		// return criteria.list();
		return null;
	}

	@Override
	public Collection<T> findManyByCriteria(CriteriaQuery<T> criteria) {
		return em.createQuery(criteria).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<T> findManyByQuery(String jpaQuery) {
		return em.createQuery(jpaQuery).getResultList();
	}

	@Override
	public boolean create(T entity) {

		try {
			em.persist(entity);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean createMany(Collection<T> setOfEntities) {
		try {
			Iterator<T> it = setOfEntities.iterator();
			while (it.hasNext())
				em.persist(it.next());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean update(T newEntity) {
		try {
			em.merge(newEntity);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateMany(Collection<T> entities) {
		try {
			Iterator<T> it = entities.iterator();
			while (it.hasNext())
				em.merge(it.next());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}

	@Override
	public boolean delete(T entity) {
		try {
			em.remove(em.merge(entity));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean delete(ID id) {
		T entity = (T) em.find(persistentClass, id);
		try {
			em.remove(em.merge(entity));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean deleteMany(Collection<T> entitiesToDelete) {
		throw new UnsupportedOperationException("Not Implemented");
	}

	@Override
	public boolean deleteManyByID(Collection<ID> ids) {
		throw new UnsupportedOperationException("Not Implemented");
	}

	@Override
	public boolean deleteAll() {
		throw new UnsupportedOperationException("Not Implemented");
	}

	@Override
	public void setEntityManager(EntityManager em) {
		if (em == null)
			this.em = em;
	}

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

}
