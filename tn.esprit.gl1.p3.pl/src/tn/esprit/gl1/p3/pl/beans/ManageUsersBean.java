package tn.esprit.gl1.p3.pl.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import tn.esprit.gl1.p3.dal.entity.Profile;
import tn.esprit.gl1.p3.ejb.service.remote.ProfileServiceRemote;

@ManagedBean
@ViewScoped
public class ManageUsersBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 251952265102626361L;
	@EJB
	private ProfileServiceRemote profileservice;
	
	private Profile profile = new Profile();

	private List<Profile> profiles=new ArrayList<Profile>();
	
	
	
	public List<Profile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

	public ProfileServiceRemote getProfileservice() {
		return profileservice;
	}

	public void setProfileservice(ProfileServiceRemote profileservice) {
		this.profileservice = profileservice;
	}

	public ManageUsersBean() {
		// TODO Auto-generated constructor stub
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String doUpdate(){
		
		profileservice.update(profile);
		profile = new Profile();
		profiles = (List<Profile>) profileservice.findAll();
		return null;
	}
	
	public void doDelete() {
		profileservice.delete(profile);
		profiles = (List<Profile>) profileservice.findAll();
	}
    
	@PostConstruct
	public void init() {
		profiles = (List<Profile>) profileservice.findAll();
	}

}
