package tn.esprit.gl1.p3.pl.beans;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import tn.esprit.gl1.p3.dal.entity.Address;
import tn.esprit.gl1.p3.dal.entity.Company;
import tn.esprit.gl1.p3.ejb.service.remote.CompanyServiceRemote;

@ManagedBean(name="companyRegistration")
@ViewScoped
public class CompanyRegistrationBean {
	
	@EJB
	private CompanyServiceRemote companyService;
	
	private Company company = new Company();
	private Address address = new Address();
	
	public CompanyRegistrationBean() {
		// TODO Auto-generated constructor stub
	}
	
	public String register(){
		company.setAddress(address);
		boolean success = companyService.create(company);
		if(!success){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Registration failed"));
			return null;
		}
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Registration Succeeded"));
		return "/views/login/index.xhtml";
	}
	
	public Company getCompany() {
		return company;
	}
	
	public void setCompany(Company company) {
		this.company = company;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
}
