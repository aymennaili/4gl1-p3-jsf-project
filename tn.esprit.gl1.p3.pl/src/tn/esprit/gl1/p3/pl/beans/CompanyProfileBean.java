package tn.esprit.gl1.p3.pl.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.jms.IllegalStateException;

import org.jboss.logging.Logger;

import tn.esprit.gl1.p3.dal.entity.Company;
import tn.esprit.gl1.p3.dal.entity.Student;
import tn.esprit.gl1.p3.dal.entity.SuggestedProfile;
import tn.esprit.gl1.p3.ejb.service.remote.CompanyServiceRemote;

@ManagedBean(name = "compBean")
@SessionScoped
public class CompanyProfileBean implements Serializable {

	/**
	 * 
	 */

	Logger log = Logger.getLogger(this.getClass().getName());

	@EJB
	private CompanyServiceRemote companyServiceRemote;

	@ManagedProperty("#{loginBean}")
	private LoginBean loginBean;

	private static final long serialVersionUID = 6223559196606581767L;

	private Company company;
	private List<String> images;
	private List<SuggestedProfile> suggestedProfiles; 
	private Student student;
	
	public CompanyProfileBean() {
		
		images = new ArrayList<String>();
		images.add("Image5.jpg");
		images.add("Image6.jpg");
		images.add("Image2.jpg");
		images.add("Image3.jpg");
		images.add("Image4.jpg");

	}

	@PostConstruct
	public void initUserInstance() {
		company = companyServiceRemote.findOneByEmail(loginBean.getEmail());
		if (company == null)
			try {
				throw new IllegalStateException(
						"No Company found with given email : "
								+ loginBean.getEmail());
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
	}

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public List<SuggestedProfile> getSuggestedProfiles() {
		return suggestedProfiles;
	}

	public void setSuggestedProfiles(List<SuggestedProfile> suggestedProfiles) {
		this.suggestedProfiles = suggestedProfiles;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

}
