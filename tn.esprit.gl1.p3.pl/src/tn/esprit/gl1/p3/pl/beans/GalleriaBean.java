package tn.esprit.gl1.p3.pl.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

@ManagedBean(name = "galleria")
@ViewScoped
public class GalleriaBean {

	private List<String> images;
	private Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	@PostConstruct
	public void init() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		images = new ArrayList<String>();
		/*int nbOfImg = 1;
		log.info("Request URI : "+request.getRequestURI());
		log.info("ServletContext URI : "+request.getServletContext().getContextPath());
		log.info("Exploring : "+"http://"+request.getServerName()+":"+request.getLocalPort()+request.getContextPath()+"/resources/image");
		File imageDir = new File("http://"+request.getServerName()+":"+request.getLocalPort()+request.getContextPath()+"/resources/image");
		nbOfImg = imageDir.listFiles().length;
		log.info("Retrived " + nbOfImg + " images to display");
		nbOfImg = nbOfImg == 0 ? 12 : nbOfImg;*/
		for (int i = 1; i <= 4; i++) {
			images.add("Image" + i + ".jpg");
		}
	}

	public List<String> getImages() {
		return images;
	}
}
