package tn.esprit.gl1.p3.pl.beans;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.jboss.resteasy.spi.HttpResponse;

import tn.esprit.gl1.p3.dal.entity.Address;
import tn.esprit.gl1.p3.dal.entity.CurriculumVitae;
import tn.esprit.gl1.p3.dal.entity.Student;
import tn.esprit.gl1.p3.ejb.service.remote.StudentServiceRemote;

@ViewScoped
@ManagedBean(name = "studentRegistration")
public class StudentRegistrationBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3716050955457795712L;

	@EJB
	private StudentServiceRemote studentService;

	private Student student = new Student();
	private Address address = new Address(0,"N/A","N/A","N/A","N/A","(00) 000-000");
	private String password;
	private String passwordConfirmation;

	public StudentRegistrationBean() {

	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void validatePasswordsMatch() {
		if (!password.equals(passwordConfirmation)) {
			FacesContext.getCurrentInstance().addMessage(
					"password",
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Registration failed",
							"The passwords does not match"));
			FacesContext.getCurrentInstance().addMessage(
					"passwordConfirmation",
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Registration failed",
							"The passwords does not match"));
		}
	}

	public String Register() {
		if (!password.equals(passwordConfirmation)) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Registration failed",
							"The passwords does not match"));
			return null;
		}
		student.setPassword(password);
		student.setAddress(address);
		student.setCV(new CurriculumVitae());
		boolean success = studentService.create(student);
		if (!success) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Registration failed", "Please try again"));
			return null;
		}
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Registration succeded",
						"Student succesfully registred"));
		return "/views/login/index.xhtml";
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}

}
