package tn.esprit.gl1.p3.pl.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.gl1.p3.dal.entity.Administrator;

@ManagedBean
@ViewScoped
public class AdminSpaceBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4035897040628109728L;
	
	public Administrator administrator=new Administrator();

	public AdminSpaceBean() {
		// TODO Auto-generated constructor stub
	}
	
	public Administrator getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}

	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
