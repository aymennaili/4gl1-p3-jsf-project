package tn.esprit.gl1.p3.pl.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.gl1.p3.dal.entity.Job;
import tn.esprit.gl1.p3.ejb.service.remote.JobServiceRemote;

@ManagedBean
@ViewScoped
public class JobBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6625827846118535215L;
	@EJB
	public JobServiceRemote jobService;
	public Job job = new Job();
	public List<Job> jobs = new ArrayList<Job>();

	public JobBean() {
		// TODO Auto-generated constructor stub
	}
	
	public List<Job> getJobs() {
		return jobs;
	}
	
	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}
	
	public JobServiceRemote getJobService() {
		return jobService;
	}

	public void setJobService(JobServiceRemote jobService) {
		this.jobService = jobService;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String addJob() {
		jobService.create(job);
		job=new Job();
		jobs=(List<Job>) jobService.findAll();
         return null;
	}
	
	public String deleteJob(){
		jobService.delete(job);
		job=new Job();
		jobs=(List<Job>) jobService.findAll();
		return null;
	}
	@PostConstruct
	public void init(){
		
		jobs=(List<Job>) jobService.findAll();
	}
}
