package tn.esprit.gl1.p3.pl.beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import tn.esprit.gl1.p3.dal.entity.Address;
import tn.esprit.gl1.p3.dal.entity.Administrator;
import tn.esprit.gl1.p3.dal.entity.Company;
import tn.esprit.gl1.p3.dal.entity.CurriculumVitae;
import tn.esprit.gl1.p3.dal.entity.Degree;
import tn.esprit.gl1.p3.dal.entity.Interest;
import tn.esprit.gl1.p3.dal.entity.Internship;
import tn.esprit.gl1.p3.dal.entity.Keyword;
import tn.esprit.gl1.p3.dal.entity.Language;
import tn.esprit.gl1.p3.dal.entity.Profile;
import tn.esprit.gl1.p3.dal.entity.Student;
import tn.esprit.gl1.p3.ejb.service.remote.ProfileServiceRemote;
import tn.esprit.gl1.p3.ejb.service.remote.StudentServiceRemote;

@ManagedBean
@SessionScoped
public class LoginBean implements Serializable {
	/**
	 * 
	 */
	@EJB
	private ProfileServiceRemote profileService;
	@EJB
	private StudentServiceRemote studentService;

	Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
	private static final long serialVersionUID = -3616789981996552128L;
	private String email;
	private String password;
	private Profile user;
	
	public LoginBean() {
		// TODO Auto-generated constructor stub
	}

	public String authenticate() {
		user = profileService.findByEmailAndPassword(email, password);
		if (user == null) {
			FacesContext
					.getCurrentInstance()
					.addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"Authentication failed",
									"Incorrect username or password, please try again"));
			return null;
		}
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true);
		if (user instanceof Student) {
			session.setAttribute("user", "student");
			return "/views/dashboard/student/index.jsf?faces-redirect=true";
		} else if (user instanceof Company) {
			session.setAttribute("user", "company");
			return "/views/dashboard/company/index.jsf?faces-redirect=true";
		} else if (user instanceof Administrator) {
			session.setAttribute("user", "administrator");
			return "/views/admin/adminspace.jsf?faces-redirect=true";
		}
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR", "Internal Error"));
		return null;
	}
	

	public void disconnect(){
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		HttpSession session = request.getSession(false);
		session.setAttribute("user", null);
//		session.invalidate();
		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		try {
			response.sendRedirect(request.getContextPath()+"/views/login/index.jsf");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@PostConstruct
	public void addStudents() {
		Address ghassenChermitiAddress;
		Student ghassenChermiti, amineBennia;
		CurriculumVitae ghassenChermitiCv;

		ghassenChermiti = new Student("ghassen.chermiti", "chermiti",
				"Ghassen", "Chermiti", Calendar.getInstance().getTime(),
				"Software Engineering Student", "ghassen.chermiti@esprit.tn");
		ghassenChermitiAddress = new Address(12, "Hannibal", "Ariana",
				"Tunisia", "2083", "22544887");
		ghassenChermitiCv = new CurriculumVitae();
		ghassenChermitiCv.getDegrees().add(
				new Degree("Baccalaureat", 2008, "A Hich School"));
		ghassenChermitiCv.getDegrees()
				.add(new Degree("Bachelor", 2011, "ISET"));
		ghassenChermitiCv.getInterests().add(new Interest("Soccer"));
		Calendar startDate = Calendar.getInstance();
		startDate.set(2011, 02, 01);
		Calendar endDate = Calendar.getInstance();
		endDate.set(2011, 06, 28);
		ghassenChermitiCv.getInternships().add(
				new Internship(startDate.getTime(), endDate.getTime(),
						"OXIA Group", "PHP Developer",
						"CMS development and integration"));
		ghassenChermitiCv.getLanguages().add(
				new Language("Arabic", "Mother tongue"));
		ghassenChermitiCv.getLanguages().add(
				new Language("French", "Excellent, fluent"));
		ghassenChermitiCv.getLanguages().add(new Language("English", "Good"));
		ghassenChermitiCv.getSkills().add(new Keyword("Java"));
		ghassenChermitiCv.getSkills().add(new Keyword("PHP"));
		ghassenChermitiCv.getSkills().add(new Keyword("HTML"));
		ghassenChermitiCv.getSkills().add(new Keyword("C/C++"));

		ghassenChermiti.addInfo("ESPRIT", null, null, ghassenChermitiCv,
				ghassenChermitiAddress);

		amineBennia = new Student("amine.bennia", "bennia", "Amine", "Bennia",
				Calendar.getInstance().getTime(),
				"Clound computing engineering student",
				"amine.bennia@esprit.tn");
		amineBennia.addInfo("ENSI", null, null, null, null);
		// studentService.create(ghassenChermiti);
		// studentService.create(amineBennia);
	}
}
