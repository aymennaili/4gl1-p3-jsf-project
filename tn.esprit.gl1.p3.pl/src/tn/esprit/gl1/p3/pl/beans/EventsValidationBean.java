package tn.esprit.gl1.p3.pl.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import tn.esprit.gl1.p3.dal.entity.Degree;
import tn.esprit.gl1.p3.dal.entity.Event;
import tn.esprit.gl1.p3.dal.entity.Profile;
import tn.esprit.gl1.p3.ejb.service.remote.EventServiceRemote;

@ManagedBean
@ViewScoped
public class EventsValidationBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6630088843372516135L;

	@EJB
	private EventServiceRemote eventService;
	
	private Event event = new Event();
	
	private List<Event> events = new ArrayList<Event>();
	
	private Event selectedEvent;

	public EventsValidationBean() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Event getSelectedEvent() {
		return selectedEvent;
	}


	public void setSelectedEvent(Event selectedEvent) {
		this.selectedEvent = selectedEvent;
	}


	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EventServiceRemote getEventService() {
		return eventService;
	}

	public void setEventService(EventServiceRemote eventService) {
		this.eventService = eventService;
	}
	
	public String addEvent(){
		
		eventService.create(event);
		event =new Event();
		events=(List<Event>) eventService.findAll();
		return null;
		
	}
	
	public String doUpdate(){
		
		eventService.update(event);
		event =new Event();
		events=(List<Event>) eventService.findAll();
		return null;
	}
	
	public String doDelete(){
		eventService.delete(event);
		event =new Event();
		events = (List<Event>) eventService.findAll();
		return null;
	}
	
	@PostConstruct
	public void init() {
		events = (List<Event>) eventService.findAll();
	}
	
}
