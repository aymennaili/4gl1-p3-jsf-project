package tn.esprit.gl1.p3.pl.beans;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.stream.FileImageOutputStream;
import javax.jms.IllegalStateException;
import javax.servlet.ServletContext;

import org.jboss.logging.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import tn.esprit.gl1.p3.dal.entity.Degree;
import tn.esprit.gl1.p3.dal.entity.Internship;
import tn.esprit.gl1.p3.dal.entity.Student;
import tn.esprit.gl1.p3.ejb.service.remote.DegreeServiceRemote;
import tn.esprit.gl1.p3.ejb.service.remote.InternshipServiceRemote;
import tn.esprit.gl1.p3.ejb.service.remote.StudentServiceRemote;

@ManagedBean(name = "studentProfile")
@ViewScoped
public class StudentProfileBean implements Serializable {
	/**
	 * 
	 */
	Logger log = Logger.getLogger(this.getClass().getName());
	private static String UPLOAD_PATH;
	
	@EJB
	private StudentServiceRemote studentService;

	@EJB
	private DegreeServiceRemote degreeService;
	@EJB
	private InternshipServiceRemote internshipService;
	@ManagedProperty("#{loginBean}")
	private LoginBean loginBean;

	private static final long serialVersionUID = -6381519733775538838L;
	private Student user;
	private List<Degree> degrees;
	private List<Internship> internships;
	private Degree selectedDegree;
	private Degree newDegree;
	private Internship selectedInternship = new Internship();
	private Internship newInternship = new Internship();
	private String selectedPhoto;
	private String croppedPhoto;

	public StudentProfileBean() {
	}

	@PostConstruct
	public void initUserInstance() {
		user = studentService.findOneByEmail(loginBean.getEmail());
		if (user == null)
			try {
				throw new IllegalStateException(
						"No Student found with given email : "
								+ loginBean.getEmail());
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
		ServletContext context = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		UPLOAD_PATH = context.getRealPath("")+"\\resources\\image\\upload\\";
	}
	
	
	

	public String updateDegree() {
		boolean success = degreeService.update(selectedDegree);
		FacesMessage successMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Updated", "Degree updated successfully");
		FacesMessage errorMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"Failed", "Degree update failed, try again");
		if (success)
			FacesContext.getCurrentInstance().addMessage(null, successMsg);
		else
			FacesContext.getCurrentInstance().addMessage(null, errorMsg);
		return null;
	}

	public void saveProfileChanges() {
		log.info("Saving changes");
		boolean success = studentService.update(user);
		FacesMessage successMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Updated", "Changes saved successfully");
		FacesMessage errorMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"Failed", "Update failed, try again");
		if (success)
			FacesContext.getCurrentInstance().addMessage(null, successMsg);
		else
			FacesContext.getCurrentInstance().addMessage(null, errorMsg);
	}

	public void addNewDegree() {
		// if(null==newDegree) return;
		newDegree.setCV(user.getCV());
		boolean success = degreeService.create(newDegree);
		FacesMessage successMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Created", "Degree added successfully");
		FacesMessage errorMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"Failed", "New degree creation failed, please try again");
		if (success)
			FacesContext.getCurrentInstance().addMessage(null, successMsg);
		else
			FacesContext.getCurrentInstance().addMessage(null, errorMsg);
		newDegree = new Degree();
		// return null;
	}

	public void deleteSelectedDegree() {
		log.info("Deleting degree " + selectedDegree.getDegreeId());
		boolean success = degreeService.delete(selectedDegree);
		FacesMessage successMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Deleted", "Degree deleted successfully");
		FacesMessage errorMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"Failed", "Degree deletion failed, please try again");
		if (success)
			FacesContext.getCurrentInstance().addMessage(null, successMsg);
		else
			FacesContext.getCurrentInstance().addMessage(null, errorMsg);
	}

	public void editObjective() {
		boolean success = studentService.update(user);
		FacesMessage successMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Success", "Objective updated successfully");
		FacesMessage errorMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"Failed", "Objective update failed, please try again");
		if (success)
			FacesContext.getCurrentInstance().addMessage(null, successMsg);
		else
			FacesContext.getCurrentInstance().addMessage(null, errorMsg);
	}

	public void deleteObjective() {
		user.getCV().setObjective(null);
		boolean success = studentService.update(user);
		FacesMessage successMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Success", "Objective deleted");
		FacesMessage errorMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"Failed", "Objective deletion failed, please try again");
		if (success)
			FacesContext.getCurrentInstance().addMessage(null, successMsg);
		else
			FacesContext.getCurrentInstance().addMessage(null, errorMsg);
	}

	public void deleteSelectedInternship() {
		log.info("Deleting Internship " + selectedInternship.getInternId());
		boolean success = internshipService.delete(selectedInternship);
		FacesMessage successMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Deleted", "Internship deleted successfully");
		FacesMessage errorMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"Failed", "Internship deletion failed, please try again");
		if (success)
			FacesContext.getCurrentInstance().addMessage(null, successMsg);
		else
			FacesContext.getCurrentInstance().addMessage(null, errorMsg);
	}

	public void addNewInternship() {
		if (newInternship.getEntrepriseName().isEmpty())
			return;
		newInternship.setCV(user.getCV());
		boolean success = internshipService.create(newInternship);
		FacesMessage successMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Created", "Internship added successfully");
		FacesMessage errorMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"Failed", "New internship creation failed, please try again");
		if (success)
			FacesContext.getCurrentInstance().addMessage(null, successMsg);
		else
			FacesContext.getCurrentInstance().addMessage(null, errorMsg);
	}

	public void editSeletectedInternship() {
		internshipService.update(selectedInternship);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage("Done", "Internship edited"));
	}
	
	public void handlePhotoUpload(FileUploadEvent event) {  
		log.info("Uploading profile picture");
		UploadedFile uploadedPhoto = event.getFile();
		String newPhotoName = "photo_"+user.getEmail();
		File newPhoto = new File(UPLOAD_PATH+newPhotoName);
		log.info(newPhotoName);
		try {
			newPhoto.createNewFile();
			FileImageOutputStream stream = new FileImageOutputStream(newPhoto);
			stream.write(uploadedPhoto.getContents(), 0, uploadedPhoto.getContents().length);
		} catch (IOException e) {
			e.printStackTrace();
			FacesMessage msg = new FacesMessage("Failed", "Internal Error");  
	        FacesContext.getCurrentInstance().addMessage(null, msg);
			return;
		}
		user.setProfilePhoto(newPhotoName);
		studentService.update(user);
        FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");  
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
	
	public void removeProfilePicture(){
		user.setProfilePhoto(null);
		studentService.update(user);
	}

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	public Student getUser() {
		return user;
	}

	public void setUser(Student user) {
		this.user = user;
	}

	public String getPhoto() {
		if (user.getProfilePhoto()==null || user.getProfilePhoto().isEmpty())
			return "No-Picture.jpg";
		else return user.getProfilePhoto();
	}

	public List<Degree> getDegrees() {
		degrees = degreeService.findByCv(user.getCV());
		return degrees;
	}

	public void setDegrees(List<Degree> degrees) {
		this.degrees = degrees;
	}

	public Degree getSelectedDegree() {
		return selectedDegree;
	}

	public void setSelectedDegree(Degree selectedDegree) {
		this.selectedDegree = selectedDegree;
	}

	public Degree getNewDegree() {
		if (null == newDegree)
			newDegree = new Degree();
		return newDegree;
	}

	public void setNewDegree(Degree newDegree) {
		this.newDegree = newDegree;
	}

	public List<Internship> getInternships() {
		internships = internshipService.findByCv(user.getCV());
		return internships;
	}

	public void setInternships(List<Internship> internships) {
		this.internships = internships;
	}

	public Internship getSelectedInternship() {
		return selectedInternship;
	}

	public void setSelectedInternship(Internship selectedInternship) {
		this.selectedInternship = selectedInternship;
	}

	public Internship getNewInternship() {
		return newInternship;
	}

	public void setNewInternship(Internship newInternship) {
		this.newInternship = newInternship;
	}

	public String getSelectedPhoto() {
		return selectedPhoto;
	}

	public void setSelectedPhoto(String selectedPhoto) {
		this.selectedPhoto = selectedPhoto;
	}

	public String getCroppedPhoto() {
		return croppedPhoto;
	}

	public void setCroppedPhoto(String croppedPhoto) {
		this.croppedPhoto = croppedPhoto;
	}

	

}
