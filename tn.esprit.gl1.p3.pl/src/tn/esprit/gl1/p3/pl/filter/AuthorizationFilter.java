package tn.esprit.gl1.p3.pl.filter;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/views/*")
public class AuthorizationFilter implements Filter {

	Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String requestedResource = request.getRequestURI();
		HttpSession session = request.getSession(false);
		log.info("Filtering " + requestedResource);
		boolean isAuthenticated = (session !=null && session.getAttribute("user") != null);
		boolean isAccessingLogin = requestedResource
				.contains("login");
		boolean isAccessingRegistration = requestedResource
				.contains("register");

		if (!isAuthenticated) {
			// User is not authenticated
			if (isAccessingLogin || isAccessingRegistration) {
				log.info("Login or registration access");
				chain.doFilter(req, res);
			} else {
				log.info("Unauthorized access ... redirecting to login page");
				response.sendRedirect(request.getContextPath()
						+ "/views/login/index.jsf?faces-redirect=true");
			}
		} else {
			// User is authenticated
			if (isAccessingLogin || isAccessingRegistration) {
				String userType = (String) session.getAttribute("user");
				if (userType.equals("student")) {
					log.info("User already connected ... redirecting to student dashboard");
					response.sendRedirect(request.getContextPath()
							+ "/views/dashboard/student/index.jsf?faces-redirect=true");
				} else if (userType.equals("company")) {
					log.info("User already connected ... redirecting to company dashboard");
					response.sendRedirect(request.getContextPath()
							+ "/views/dashboard/company/index.jsf?faces-redirect=true");
				} else if (userType.equals("administrator")) {
					log.info("User already connected ... redirecting to administrator dashboard");
					response.sendRedirect(request.getContextPath()
							+ "/views/admin/adminspace.jsf?faces-redirect=true");
				}
			} else {
				log.info("Authorized request");
				chain.doFilter(req, res);
			}
		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
	}

}
